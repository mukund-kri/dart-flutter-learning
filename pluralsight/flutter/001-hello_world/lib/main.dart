import 'package:flutter/material.dart';

import './screens/home.dart';


void main() => runApp(HelloWorldApp());

class HelloWorldApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp (
      title: "Flutter Hello world app",
      home: Scaffold(
        appBar: AppBar(
          title: Text("HelloWorld app App Bar"),
        ),
        body: HomeWidget(),
      ),
    );

  }


}
