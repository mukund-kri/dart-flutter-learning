import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HelloYou(),
    );
  }
}

class HelloYou extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HelloYouState();
}

class _HelloYouState extends State<StatefulWidget> {

  String name = '';
  final _currencies = <String>["Dollar", "Euro", "Pound"];
  var _selectedCurrency = "Dollar";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Stateful demo",),
        backgroundColor: Colors.blueAccent,
      ),
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[
            TextField(
              decoration: InputDecoration(
                hintText: "Enter Your Name ...",
              ),
              onSubmitted: (String str) {
                setState(() {
                  name = str;
                });
              },
            ),

            DropdownButton<String>(
              items: _currencies.map((String value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              value: _selectedCurrency,
              onChanged: (String value) {
                _onCurrencySelect(value);
              },
            ),

            Text(
              "Hello $name!"
            ),
          ],
        ),
      ),
    );
  }

  void _onCurrencySelect(String  value) {
    setState(() {
      _selectedCurrency = value;
    });
  }
}