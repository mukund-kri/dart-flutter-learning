import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fuel calculator',
      home: FuelForm()
    );
  }
}

class FuelForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FuelFormState();
}

class _FuelFormState extends State<FuelForm> {

  final _formKey = GlobalKey<FormState>();

  String result = "";
  String _currency = "";
  final _currencies = ['Dollars', 'Euro', 'Pounds', 'Yen'];

  TextEditingController _distanceController = TextEditingController();
  TextEditingController _avgController = TextEditingController();
  TextEditingController _fuelController = TextEditingController();


  @override
  void initState() {
    this._currency = _currencies[0];
  }


  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    double _formPadding = 5.0;


    return Scaffold(
      appBar: AppBar(
        title: Text('Fuel Calculator'),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.only(left: _formPadding, right: _formPadding),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: _formPadding, bottom: _formPadding),
                child: TextField(
                  controller: _distanceController,
                  style:  textStyle,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: "e.g 248",
                    labelText: "Distance",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(0.5)
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: _formPadding, bottom: _formPadding
                ),
                child: TextField(
                  controller: _avgController,
                  keyboardType: TextInputType.number,
                  style: textStyle,
                  decoration: InputDecoration(
                    hintText: "e.g 248",
                    labelText: "Distance per Unit",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(0.5),
                    ),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.only(
                  left: _formPadding, right: _formPadding,
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        controller: _fuelController,
                        style: textStyle,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: "e.g 1.2",
                          labelText: "Fuel Cost",
                          labelStyle: textStyle,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0.5)
                          )
                        ),
                      )
                    ),
                    Container(width: _formPadding * 5,),
                    Expanded(
                      child: DropdownButton<String>(
                        items: _currencies.map( (String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        value: _currency,
                        onChanged: (value) {
                          _onDropDownChanged(value);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: _formPadding,
              ),
              
            ],
          ),
        ),
      )
    );
  }


  void _onDropDownChanged(String value) {
    setState(() {
      print(value);
      this._currency = value;
    });
  }

}