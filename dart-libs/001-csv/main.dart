import 'dart:convert';
import 'dart:io';

import 'package:csv/csv.dart';


void main() async {

  // Open up a csv file as a stream
  final input = new File('mca_westbengal_min.csv').openRead();

  final csv = input.transform(utf8.decoder).transform(new CsvToListConverter()).asBroadcastStream();


  final sub1 = csv.listen((row) => print(row[8]));

  final sub2 = csv.listen((row) => print(row));

  sub1.onDone(() => print('Done with stream1 '));
}