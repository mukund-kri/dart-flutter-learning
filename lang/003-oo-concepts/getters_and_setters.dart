
void main() {
  var aPerson = new Person('John', 'Doe');
  
  // Testing the getter
  print(aPerson.fullName);

  // Testing the setter
  aPerson.fullName = 'Jane, Doe';
  print(aPerson.fullName);
}

class Person {

  // A couple of private variables
  String _firstName;
  String _lastName;

  // Constructor sets the firstName and lastName
  Person(this._firstName, this._lastName);

  // getter called fullName
  String get fullName => "$_firstName, $_lastName";

  // also a setter for full name. A bit convoluted but 
  // shows the concept
  set fullName(String fName) {
    var split = fName.split(', ');
    _firstName = split[0];
    _lastName = split[1];
  }
}