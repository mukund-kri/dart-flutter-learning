import 'package:http/http.dart' as http;


void main() {

  final url = 'http://example233.com';

  // the http.get method returns a Future
  final responseFuture = http.get(url);

  responseFuture
    .then((r) {
      // the event loop will execut this when the response is ready
      print(r.body);
    })
    .catchError( (err) {
      // if the future results in an error, handle it here
      print('Caught $err');
    })
    .whenComplete(() {
      // will allways execute, in cases of Success or Failure
      print('Done with the Future');
    });

  // This should execute first
  print('Basic Furture example with the http lib');
}