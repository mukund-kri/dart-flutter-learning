void main() {

  final aFuture = Future<int>.delayed(
    Duration(seconds: 3),
    // () =>  100,                  // the future is successful
    () => throw Exception(),        // I want the future to error out
  );

  aFuture.then((int value) {
    print('Got value $value');
  })
  .catchError((error) {
    print('Error $error happened');
  });
}