// Demo of how to :: field access and nested access
// From the dart guide


class PhoneNumber {
  String number;
  String label;

  PhoneNumber(this.number, this.label) {}

  String toString() {
    return '$label :: $number';
  }
}

class PhoneNumberBuilder {
  String area_code;
  String number;
  String label;

  PhoneNumber build() {
    return PhoneNumber(
      '$area_code-$number',
      label,
    );
  }
}


class AddressBook {
  String name;
  String email;

  PhoneNumber phone_number;

  String toString() {
    return '''
Name :: $name 
Email :: $email
$phone_number''';
  }
}


void main() {

  final address = AddressBook()
    ..name = 'jenny'
    ..email = 'jenny@example.com'
    ..phone_number = (PhoneNumberBuilder()
      ..area_code = '415'
      ..number = '555-0100'
      ..label = 'home')
    .build();

  print(address);
}
