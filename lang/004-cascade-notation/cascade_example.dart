// This file explains the cascade notation in dart.
// Most of the code is from the following stackoverflow page
// https://stackoverflow.com/questions/49447736/list-use-of-double-dot-in-dart


void main() {

  // Traditionally in c syntax you would do this
  List<String> names = [];
  names.add('John');
  names.add('James');
  names.add('Jan');
  names.add('Jody');
  print(names);
  
  // But with cascase syntax
  List<String> names_c = [];
  names_c
  ..add('John')
  ..add('James')
  ..add('Jan')
  ..add('Jody');
  print(names_c);

  // Or better still ...
  final name_cs = new List<String>()
  ..add('John')
  ..add('James');

  print(name_cs);
}
