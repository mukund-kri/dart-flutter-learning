import 'package:moor/moor.dart';


part 'model.g.dart';


class Todos extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get title => text().withLength(min: 6, max: 32)();
}


@UseMoor(tables: [Todos])
class MyDatabase {
}
