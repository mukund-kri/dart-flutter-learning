import 'package:flutter/material.dart';
import 'package:provider_router/models/journalDay.dart';

class JournalsState extends ChangeNotifier {

  List<JournalDay> journalDays = <JournalDay>[];


  void addJournalDay(JournalDay journalDay) {
    this.journalDays.add(journalDay);
    notifyListeners();
  }

}
