import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_provider/models/textModel.dart';

import './ui/eventProducer.dart';
import './ui/eventConsumer.dart';
import './models/textModel.dart';


void main() => runApp(ProviderApp());

class ProviderApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider<TextModel>(
      create: (context) => TextModel(),
      child: MaterialApp(
        title: 'Provider Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          appBar: AppBar(title: Text('Provider Demo'),),
          body: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              EventProducer(),

              EventConsumer(),
            ],
          ),
        ),
      ),
    );
  }
}
