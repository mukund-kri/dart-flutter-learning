import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_provider/models/textModel.dart';

class EventConsumer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(35),
      color: Colors.blue[200],
      child: Consumer<TextModel>(
        builder: (context, model, child) {
          return Text(model.greeting);
        },
      ),
    );
  }

}