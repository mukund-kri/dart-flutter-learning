import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_provider/models/textModel.dart';

class EventProducer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      color: Colors.green[300],
      child: Consumer<TextModel>(
        builder: (context, model, child) {
          return RaisedButton(
              child: Text('Do Something ...'),
              onPressed: () {
                model.doSomthing();
              }
          );
        },
        ),
      );
  }
}