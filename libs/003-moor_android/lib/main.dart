import 'package:flutter/material.dart';
import 'package:moor_android/model.dart';

import 'globals.dart' as globals;


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => Listing(),
        '/add': (context) => AddTodo(),
      },
    );
  }
}

class Listing extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _ListingState();
}

class _ListingState extends State<Listing> {

  Future<List<Todo>> _todosFuture;

  @override
  void initState() {
    super.initState();
    _todosFuture = globals.db.allTodoEntries;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Basic Moor app'),
      ),
      body: FutureBuilder<List<Todo>>(
        future: _todosFuture,
        builder: (context, snapshot) {
          if(snapshot.hasData) {
            return Text('Success');
          }

          print(snapshot.data);
          print(snapshot.error);

          return Text('Error');
        },
      ),
    );
  }
}

class AddTodo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add todo'),
      ),
      body: Text('Add tod'),
    );
  }
}