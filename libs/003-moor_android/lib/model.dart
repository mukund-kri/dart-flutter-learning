import 'dart:async';
// import 'dart:ffi';
import 'dart:io';


import 'package:moor/moor.dart';
import 'package:moor_ffi/moor_ffi.dart';

part 'model.g.dart';


class Todos extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get title => text().withLength(min: 6, max: 32)();
}


@UseMoor(tables: [Todos])
class MyDatabase extends _$MyDatabase {
  MyDatabase() : super(VmDatabase(File('./app.db')));

  @override
  int get schemaVersion => 1;

  Future<List<Todo>> get allTodoEntries => select(todos).get();

  Future<int> addTodo(TodosCompanion entry) {
    return into(todos).insert(entry);
  }
}
